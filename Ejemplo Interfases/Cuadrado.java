import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

public class Cuadrado implements Figura, ActionListener, KeyListener{
	private double lado;
	
	public Cuadrado(double lado){
		setLado(lado);
	}
	
	public void setLado(double lado){
		this.lado=lado;
	}
	
	public double getLado(){
		return lado;
	}
	
	public double calcularArea(){
		return lado*lado;
	}
	
	public void actionPerformed(ActionEvent evento){
		
	}
	
	public void keyReleased(KeyEvent e){
		
	}
	
	public void keyPressed(KeyEvent e){
		
	}
	
	public void keyTyped(KeyEvent e){
		
	}
}
