public class Circulo implements Figura{
	private double radio;
	
	public Circulo(double radio){
		setRadio(radio);
	}
	
	public void setRadio(double radio){
		this.radio=radio;
	}
	
	public double getRadio(){
		return radio;
	}
	
	public double calcularArea(){
		return Math.PI*(radio*radio);
	}
}
